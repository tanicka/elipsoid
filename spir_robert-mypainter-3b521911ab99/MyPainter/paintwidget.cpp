#include "paintwidget.h"
using namespace std;

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	QImage loadedImage;
	if (!loadedImage.load(fileName))
		return false;

	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && painting) {
		drawLineTo(event->pos());
		painting = false;
	}
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.drawLine(lastPoint, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint = endPoint;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::generuj(int rov, int pol) //n-rovnobezka, m-poludnik
{
	PaintWidget::BOD bod;
	PaintWidget::TROJUHOLNIK trojuholnik;
	TROJUHOLNIK *troj;
//	PaintWidget::TELESO teleso;
	double posun_v = (M_PI) / (rov);
	double posun_t = (2 * M_PI) / (pol);
	double t = 0;
	double v = 0;
	pom2 = 0;
	int xs = image.width();
	int ys = image.height();
	int a = 1; int b = 2; int c = 3;
	pole1 = new BOD[(rov + 1)*(pol + 1)];
	troj = new TROJUHOLNIK[(rov + 1)*(pol + 1) * 2];

	for (int i = 0; i <= rov; i++)
	{
		v = 0;
		for (int j = 0; j <= pol; j++)
		{
			QPoint A;
			bod.x = a*cos(t)*sin(v);
			bod.y = b*sin(t)*sin(v);
			bod.z = c*cos(v);

			if (fabs(bod.x)<1.0e-15)
				bod.x = 0;
			if (fabs(bod.y)<1.0e-15)
				bod.y = 0;
			if (fabs(bod.z)<1.0e-15)
				bod.z = 0;

			pole1[pom2].x = (xs/2)+(bod.x)*100;
			pole1[pom2].y = (ys/2)+(bod.y)*100;
			pole1[pom2].z = (bod.z)*10;
			A.setX(pole1[pom2].x);
			A.setY(pole1[pom2].y);
			body.push_back(A);
			surZ.push_back(pole1[pom2].z);
			printf("%d %d ", body[i].x(), body[i].y());
			pom2++;

			v += posun_v;
		}
		t += posun_t;
	}

	int i = 0;
	do
	{
		troj[i].a = i;
		troj[i].b = i + 1;
		troj[i].c = i + 2 + pol;
		//	cout << troj[i].a << " " << troj[i].b << " " << troj[i].c << endl;
		indexy.push_back(troj[i].a);
		indexy.push_back(troj[i].b);
		indexy.push_back(troj[i].c);
		i++;

	} while (i != (pol + 1)*(rov));

	int k = i + pol;
	do
	{
		troj[i].a = k;
		troj[i].b = k - 1;
		troj[i].c = k - pol - 2;
		//	cout << troj[i].a << " " << troj[i].b << " " << troj[i].c<< endl;
		indexy.push_back(troj[i].a);
		indexy.push_back(troj[i].b);
		indexy.push_back(troj[i].c);
		k--;
		i++;
	} while ((k - pol - 2) != 0);
	printf("indexy %d ", indexy.size());
}

void PaintWidget::DDA(QVector<QPoint>b)
{
	QPainter painter(&image);
	QColor C(255, 0, 0);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	QMessageBox mbox;

	double dx, dy, steps, xx, yy, x, y;
	int x1, x2, y1, y2, x0, y0;
	for (int i = 0; i < b.size(); i++)
	{
		if (i != b.size() - 1)
		{
			x1 = b[i].x();
			y1 = b[i].y();
			x2 = b[i + 1].x();
			y2 = b[i + 1].y();
		}

		else
		{
			x1 = b[b.size() - 1].x();
			y1 = b[b.size() - 1].y();
			x2 = b[0].x();
			y2 = b[0].y();
		}

		dx = x2 - x1;
		dy = y2 - y1;

		if (fabs(dx) > fabs(dy))steps = fabs(dx);
		else steps = fabs(dy);

		xx = dx / steps;
		yy = dy / steps;

		x = x1;
		y = y1;

		for (int i = 1; i <= steps; i++)
		{
			painter.drawEllipse((int)x, (int)y, 2, 2);
			x += xx;
			y += yy;
		}
		painter.drawEllipse(x, y, 2, 2);
	}
	//	b.clear();
	update();
}