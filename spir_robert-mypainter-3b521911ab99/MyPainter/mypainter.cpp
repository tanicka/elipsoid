#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Generuj()
{
	paintWidget.newImage(800, 600);
	int poludniky, rovnobezky,dz;
	poludniky = ui.spinBox->value();
	rovnobezky = ui.spinBox_2->value();
	dz = ui.spinBox_3->value();

	paintWidget.generuj(rovnobezky, poludniky);
	if (ui.radioButton_3->isChecked())
	{
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			if ((i + 1) % 3 == 0)
				paintWidget.Bodiky.push_back(paintWidget.body[paintWidget.indexy[i - 2]]);
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}
	else //if(ui.radioButton_4->isChecked())
	{
		QVector<QPoint>pomocny;
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			QPoint A;
			int xx, yy;
			xx = paintWidget.body[it].x();
			yy = paintWidget.body[it].y();
			A.setX((dz*xx )/ paintWidget.surZ[it]);
			A.setY((dz*yy)/ paintWidget.surZ[it]);
			pomocny.push_back(A);
		//	if ((i + 1) % 3 == 0)
		//		paintWidget.Bodiky.push_back(pomocny[paintWidget.indexy[i - 2]]);
		//	paintWidget.Bodiky.push_back(pomocny[i]);
		//	paintWidget.Bodiky.push_back(pomocny[i]);
			
		}paintWidget.DDA(pomocny);
	}
	

}
