#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <iostream>
#include <fstream>


class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	PaintWidget(QWidget *parent = 0);

	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void generuj(int rov, int pol);
	void DDA(QVector<QPoint>b);
	void rotuj(double PHI, double THETA);
	void Phongov_osv_model(bool moznost);
	void vypln_troj();
	void zBuffer(QVector<QColor>Farba,int zsur);
	void TH(QVector<QPoint>troj);
	void sort();
	void Konstantne();
	void otocenie_vlavo(int uhol);
	void otocenie_vpravo(int uhol);
	void skalovanie(float koef);
	void skosenie(float koef, bool x, bool y);
	void preklapanie();
	void posunutie();

	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }
	bool moznost;
	QVector<QPoint>Bodiky;
	QVector<QPoint>body;
	QVector<QPoint>bodicky;
	QVector<int>indexy;
	QVector<int>surZ;
	QVector<int>Zpom;
	QVector<QVector<QColor>>F;
	QVector<QVector<int>>Z;
	QVector<QPoint>stredne;
	QVector< QVector< double > >tabulkaHran;
	QVector<QPoint>ABD;
	QVector<QPoint>ADC;
	QVector<int>ABDz;
	QVector<int>ADCz;
	QVector<QPoint>D;
	QVector<QColor>I;
	QImage image;
	int dz;
	//zrkadlova zlozka
	int ILr, ILg, ILb;
	double rs, h;
	//difuzna zlozka
	int rd;
	//ambientna zlozka
	int IOr, IOg, IOb;
	double ra;
	//transformacie
	float koef;
	int uhol;
	QVector<QPoint>body_pom;
	bool rightclick = false;
	QVector<QPoint>posunutie_body;
	QPoint lastPoint2;

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);
	

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);
	int selectKth(int* data, int s, int e, int k);

	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QPoint lastPoint;
	int pocet_poly;
	int pom2;

	typedef struct SUR
	{
		double x;
		double y;
		double z;
	}SUR;
};




#endif // PAINTWIDGET_H
