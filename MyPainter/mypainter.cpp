#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
}

MyPainter::~MyPainter()
{

}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "(*.vtk)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);

}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Generuj()
{
	paintWidget.newImage(800, 600);
	int poludniky, rovnobezky;
	poludniky = ui.spinBox->value();
	rovnobezky = ui.spinBox_2->value();
	paintWidget.dz = ui.spinBox_3->value();

	paintWidget.generuj(rovnobezky, poludniky);
	//rovnobezne premietanie
	if (ui.radioButton_3->isChecked())
	{
		paintWidget.Bodiky.clear();
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}
	//stredove premietanie
	if (ui.radioButton_4->isChecked())
	{
		int xs, ys, xx, yy;
		xs = paintWidget.image.width();
		ys = paintWidget.image.height();
		paintWidget.Bodiky.clear();
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			xx = (paintWidget.body[it].x()) - (xs / 2);
			yy = (paintWidget.body[it].y()) - (ys / 2);
			paintWidget.Bodiky.push_back(QPoint((((xx*paintWidget.dz)/(paintWidget.dz-paintWidget.surZ[it]))+(xs/2)),
				(((yy*paintWidget.dz) / (paintWidget.dz - paintWidget.surZ[it])) + (ys / 2))));
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}

	if (ui.checkBox->isChecked()) paintWidget.moznost = 0;
	if (ui.checkBox_2->isChecked()) paintWidget.moznost = 1;
//	paintWidget.vypln_troj();
}

void MyPainter::Generuj2()
{
	paintWidget.newImage(800, 600);
	//rovnobezne premietanie
	if (ui.radioButton_3->isChecked())
	{
		paintWidget.Bodiky.clear();
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}
}

void MyPainter::Rotacia()
{
	paintWidget.clearImage();
	double theta, phi;
	theta = ui.horizontalSlider->value();
	phi = ui.horizontalSlider_2->value();
	//if(ui.radioButton_3->isChecked) 
	paintWidget.rotuj(phi,theta);
	paintWidget.DDA(paintWidget.Bodiky);

	//P.O.M.
	paintWidget.ILr = ui.spinBox_4->value();
	paintWidget.ILg = ui.spinBox_5->value();
	paintWidget.ILb = ui.spinBox_6->value();
	paintWidget.IOr = ui.spinBox_7->value();
	paintWidget.IOg = ui.spinBox_8->value();
	paintWidget.IOb = ui.spinBox_9->value();
	paintWidget.rs = ui.doubleSpinBox->value();
	paintWidget.h = ui.spinBox_10->value();
	paintWidget.rd = ui.doubleSpinBox_2->value();
	paintWidget.ra = ui.doubleSpinBox_3->value();
}

void MyPainter::Vykresli()
{
	paintWidget.clearImage();
	paintWidget.Bodiky.clear();
	paintWidget.koef = ui.lineEdit->text().toFloat();
	paintWidget.uhol= ui.lineEdit_2->text().toInt();
	if (ui.comboBox->currentIndex() == 0)
	{
		paintWidget.skalovanie(paintWidget.koef);
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}

	if (ui.comboBox->currentIndex() == 1)
	{
		paintWidget.otocenie_vlavo(paintWidget.uhol);
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}

	if (ui.comboBox->currentIndex() == 2)
	{
		paintWidget.otocenie_vpravo(paintWidget.uhol);
		for (int i = 0; i < paintWidget.indexy.size(); i++)
		{
			int it = paintWidget.indexy[i];
			paintWidget.Bodiky.push_back(paintWidget.body[it]);
		}
		paintWidget.DDA(paintWidget.Bodiky);
	}


	


}
