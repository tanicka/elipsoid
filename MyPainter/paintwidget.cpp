#include "paintwidget.h"
using namespace std;

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	modified = false;
	painting = false;
	myPenWidth = 1;
	myPenColor = Qt::blue;
}

bool PaintWidget::openImage(const QString &fileName)
{
	body.clear();
	indexy.clear();
	QFile loaded(fileName);
	loaded.open(QIODevice::ReadOnly);

	QString riadok;
	QStringList slova;
	QPoint P;
	while (!riadok.contains("POINTS")) riadok = loaded.readLine();

	int pocet;
	pocet= riadok.split(" ")[1].toInt();
	printf("pocet %d",pocet);
	for (int i = 0; i < pocet; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		P.setX(slova[0].toDouble());
		P.setY(slova[1].toDouble());
		body.push_back(P);
		surZ.push_back(slova[2].toDouble());
	}
	printf("otvoreny");
	while (!riadok.contains("POLYGONS")) riadok = loaded.readLine();
	pocet_poly = riadok.split(" ")[1].toInt();

	for (int i = 0; i < pocet_poly; i++) {
		riadok = loaded.readLine();
		slova = riadok.split(" ");
		indexy.push_back(slova[1].toInt());
		indexy.push_back(slova[2].toInt());
		indexy.push_back(slova[3].toInt());
	}
	loaded.close();

	return true;
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	modified = false;
	update();
	return true;
}

bool PaintWidget::saveImage(const QString &fileName)
{
	QImage visibleImage = image;
	resizeImage(&visibleImage, size());

	if (visibleImage.save(fileName,"png")) {
		modified = false;
		return true;
	}
	else {
		return false;
	}
}

void PaintWidget::setPenColor(const QColor &newColor)
{
	myPenColor = newColor;
}

void PaintWidget::setPenWidth(int newWidth)
{
	myPenWidth = newWidth;
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	modified = true;
	update();
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
		QPainter painter(&image);
		painter.setPen(QPen(myPenColor, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(lastPoint);
		body.push_back(lastPoint);
		 
		update();
	}
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::RightButton) && painting)
		drawLineTo(event->pos());
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
		painting = true;
	}

	if (event->button() == Qt::RightButton && painting) {
		drawLineTo(event->pos());
		painting = true;
		rightclick = true;
	}

	if (rightclick == true) {
		posunutie();
		update();
	}
	rightclick = false;
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::drawLineTo(const QPoint &endPoint)
{
	QPainter painter(&image);
	//	QColor C(200, 255, 255);
	//	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	//	painter.drawLine(lastPoint2, endPoint);
	modified = true;

	int rad = (myPenWidth / 2) + 2;
	update(QRect(lastPoint2, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
	lastPoint2 = endPoint;
	posunutie_body.push_back(lastPoint2);
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

int PaintWidget::selectKth(int * data, int s, int e, int k)
{
	// 5 or less elements: do a small insertion sort
	if (e - s <= 5)
	{
		for (int i = s + 1; i < e; i++)
			for (int j = i; j > 0 && data[j - 1] > data[j]; j--) std::swap(data[j], data[j - 1]);
		return s + k;
	}

	int p = (s + e) / 2; // choose simply center element as pivot

						 // partition around pivot into smaller and larger elements
	std::swap(data[p], data[e - 1]); // temporarily move pivot to the end
	int j = s;  // new pivot location to be calculated
	for (int i = s; i + 1 < e; i++)
		if (data[i] < data[e - 1]) std::swap(data[i], data[j++]);
	std::swap(data[j], data[e - 1]);

	// recurse into the applicable partition
	if (k == j - s) return s + k;
	else if (k < j - s) return selectKth(data, s, j, k);
	else return selectKth(data, j + 1, e, k - j + s - 1); // subtract amount of smaller elements from k
}

void PaintWidget::generuj(int rovnobezky, int poludniky) //n-rovnobezka, m-poludnik
{
	body.clear();
	indexy.clear();
	vector<SUR> body_s;
	QPoint A;
	double alfa = 0, beta = 0;
	int a=10, b=15, c=20;
	int xs = image.width();
	int ys = image.height();
	rovnobezky++;

	fstream file;

	double poma = 2 * M_PI / poludniky;
	double pomb = M_PI / rovnobezky;
	SUR tmp,tmp2;

	for (int i = 0; i <= rovnobezky; i++)
	{
		alfa = 0;
		for (int j = 0; j < poludniky; j++)
		{
			tmp.x = (xs/2)+(a*cos(alfa)*sin(beta))*10.0;
			tmp.y = (ys/2)+(b*sin(alfa)*sin(beta))*10.0;
			tmp.z = (c*cos(beta))*10.0;

			body_s.push_back(tmp);
			A.setX(tmp.x);
			A.setY(tmp.y);
			body.push_back(A);
			surZ.push_back(tmp.z);

			alfa += poma;
		}
		beta += pomb;
	}

	pocet_poly = ((rovnobezky*poludniky) - poludniky) * 2;
	rovnobezky--;

	for (int i = poludniky; i < poludniky * 2; i++)
	{
		if (i == (poludniky * 2) - 1)
			//file << "3 " << poludniky << " ";
			indexy.push_back(poludniky);
		else
			//file << "3 " << i + 1 << " ";
			indexy.push_back(i + 1);

		file << i << " 0" << endl;
		indexy.push_back(i);
		indexy.push_back(0);

	}

	//file << ((poludniky*(rovnobezky + 1)) - poludniky*2)*2 << endl; 

	for (int i = poludniky; i < (pocet_poly / 2); i++)
	{
		if (i%poludniky == poludniky - 1)
		{
			i -= poludniky;
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + 2*poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + 2*poludniky);
			i += poludniky;
		}
		else
		{
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky);
			indexy.push_back(i + 1);
			indexy.push_back(i + poludniky+1);
			indexy.push_back(i + poludniky);
		}

	}

	for (int i = pocet_poly / 2; i < poludniky*(rovnobezky + 1); i++)
	{

		if (i == (poludniky*(rovnobezky + 1)) - 1)
		{
			//file << "3 " << i << " " << pocet_poly / 2 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(pocet_poly / 2);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
		else
		{
			//file << "3 " << i << " " << i + 1 << " " << poludniky*(rovnobezky + 1) << endl;
			indexy.push_back(i);
			indexy.push_back(i + 1);
			indexy.push_back(poludniky*(rovnobezky + 1));
		}
	}
	Zpom = surZ;
}

void PaintWidget::rotuj(double PHI, double THETA) {
	bodicky.clear();
	double phi = M_PI*PHI / 180.0;
	double theta = M_PI*THETA / 180.0;
	int xs = image.width();
	int ys = image.height();
	bodicky = body;
	for (int i = 0; i < Bodiky.size(); i++) 
	{
		int xx, yy;
		int it = indexy[i];
		xx = body[it].x() - (xs/2);
		yy = body[it].y() - (ys / 2);
		Bodiky[i].setX((xx*cos(phi) - yy*sin(phi))+(xs/2));
		Bodiky[i].setY((xx*sin(phi)*cos(theta) + yy*cos(phi)*cos(theta) -surZ[it]*sin(theta))+(ys/2));
		Zpom[i] = ((xx*sin(phi)*sin(theta))+(xs/2)) + ((yy*cos(theta)*sin(phi))+(ys/2)) + surZ[it]*cos(theta);
	}
}

void PaintWidget::zBuffer(QVector<QColor>Farba, int zsur)
{
	for (int i = 0; i < image.width(); i++)
	{
		for (int j = 0; j < image.height(); j++)
		{
			F[i][j] = QColor(255, 255, 255);
			Z[i][j] = INFINITY;
		}
	}

	for (int i = 0; i < pocet_poly; i++)
	{
		for (int j = 0; j < body.size(); j++)
		{
			if (Z[i][j] > zsur)
			{
				Z[i][j] = zsur;
				F[i][j] = Farba[j];
			}
		}
	}
}

void PaintWidget::Phongov_osv_model(bool moznost)
{
	
	QVector<SUR>R,Is,Id,Ia;
	SUR p_r, p_is, p_id, p_ia,vysl_i;
	double w1,w2,w3;
	//konstantne tienovanie
	if (moznost == 0)
	{
		for (int i = 0; i < Bodiky.size(); i++)
		{	
			//vypocet normaly
			//u2*v3 - u3*v2
			w1 = ((Bodiky[i + 1].y() - Bodiky[i].y())*(surZ[i + 2] - surZ[i + 1])) - ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].y() - Bodiky[i + 1].y()));
			//u3*v1 - u1*v3
			w2 = ((surZ[i + 1] - surZ[i])*(Bodiky[i + 2].x() - Bodiky[i + 1].x())) - ((Bodiky[i + 1].x() - Bodiky[i].x())*(surZ[i + 2] - surZ[i + 1]));
			//u1*v2 - u2*v1
			w3 = ((Bodiky[i + 1].x() - Bodiky[i].x())*(Bodiky[i + 2].y() - Bodiky[i + 1].y())) - ((Bodiky[i + 1].y() - Bodiky[i].y())*(Bodiky[i + 2].x() - Bodiky[i + 1].x()));

			//odrazovy luc
			p_r.x = 2 * (0 * w1)*w1 - 0;
			p_r.y = 2 * (0 * w2)*w2 - 0;
			p_r.z = 2 * (dz * w3)*w3 - dz;
			R.push_back(p_r);

			//zrkadlova zlozka
			p_is.x = ILr*rs*pow(0 * p_r.x, h);
			p_is.y = ILg*rs*pow(0 * p_r.y, h);
			p_is.z = ILb*rs*pow(0 * p_r.z, h);
			Is.push_back(p_is);

			//difuzna zlozka
			p_id.x = ILr*rd * 0 * w1;
			p_id.y = ILg*rd * 0 * w2;
			p_id.z = ILb*rd * 0 * w3;
			Id.push_back(p_id);

			//ambientna zlozka
			p_ia.x = IOr*ra;
			p_ia.y = IOg*ra;
			p_ia.z = IOb*ra;
			Ia.push_back(p_ia);

			//vysledna intenzita (asi RGB)
			vysl_i.x = p_is.x + p_id.x + p_ia.x;	//R
			vysl_i.y = p_is.y + p_id.y + p_ia.y;	//G
			vysl_i.z = p_is.z + p_id.z + p_ia.z;	//B
			QColor(vysl_i.x, vysl_i.y, vysl_i.z);
			I.push_back(QColor(vysl_i.x, vysl_i.y, vysl_i.z));
		}
	}
}

void PaintWidget::TH(QVector<QPoint>troj)
{
	tabulkaHran.clear();
	double w, x1, x2, y1, y2, dx, dy;
	for (int i = 0; i < troj.size(); i++)
	{
		//kvoli hrane posledny bod-prvy bod
		if (i != (troj.size() - 1))
		{
			x1 = troj[i].x();
			y1 = troj[i].y();
			x2 = troj[i + 1].x();
			y2 = troj[i + 1].y();
		}

		else
		{
			x1 = troj[body.size() - 1].x();
			y1 = troj[body.size() - 1].y();
			x2 = troj[0].x();
			y2 = troj[0].y();
		}

		QVector<double>row;
		//vynecha vodorovne hrany
		if ((y2 - y1) != 0)
		{
			//upravi orientaciu hran
			if (y1 > y2)
			{
				std::swap(x1, x2);
				std::swap(y1, y2);
			}
			dy = y2 - y1;
			w = (x2 - x1) / dy;
			row.push_back(y1);
			row.push_back(x1);
			row.push_back(y2 - 1);
			row.push_back(w);
			tabulkaHran.push_back(row);
		}
	}
}

void PaintWidget::sort()
{
	for (int i = 0; i < tabulkaHran.size(); i++)
	{
		for (int j = 0; j < tabulkaHran.size(); j++)
		{
			
			if (tabulkaHran[i][1] < tabulkaHran[j][1])	
			{
				QVector<double> tmp = tabulkaHran[i];
				tabulkaHran[i] = tabulkaHran[j];
				tabulkaHran[j] = tmp;
			}
	
			else if (tabulkaHran[i][1] == tabulkaHran[j][1])
			{
				if (tabulkaHran[i][3] < tabulkaHran[j][3])	
				{
					QVector<double> tmp = tabulkaHran[i];
					tabulkaHran[i] = tabulkaHran[j];
					tabulkaHran[j] = tmp;
				}
			}
		}
	}
}

void PaintWidget::vypln_troj()
{
	QPainter painter(&image);
	bodicky = body;
	QVector<int>zet;
	printf("body.size %d ", body.size());
	for (int i = 0; i < body.size(); i++)
	{
		printf("zoradovanie vrcholov \n");
		//zoradovanie vrcholov trojuholnika, aby sme nasli stredny bod
		int it = indexy[i];
		if (bodicky[it].y() > bodicky[it + 2].y())
		{
			printf("bodicky[it] %d bodicky[it+2] %d\n", bodicky[it].y(),bodicky[it+2].y());
			swap(bodicky[it], bodicky[it + 2]);
			printf("bodicky[it] %d bodicky[it+2] %d\n", bodicky[it].y(), bodicky[it + 2].y());
			zet.push_back(surZ[it + 2]);
			zet.push_back(surZ[it]);
		}

		if (bodicky[it].y() > bodicky[it + 1].y())
		{
			printf("bodicky[it] %d bodicky[it+1] %d\n", bodicky[it].y(), bodicky[it + 1].y());
			swap(bodicky[it], bodicky[it + 1]);
			printf("bodicky[it] %d bodicky[it+1] %d\n", bodicky[it].y(), bodicky[it + 1].y());
			zet.push_back(surZ[it + 1]);
			zet.push_back(surZ[it]);
		}

		if (bodicky[it + 1].y() > bodicky[it + 2].y())
		{
			printf("bodicky[it+1] %d bodicky[it+2] %d\n", bodicky[it+1].y(), bodicky[it + 2].y());
			swap(bodicky[it + 1], bodicky[it + 2]);
			printf("bodicky[it+1] %d bodicky[it+2] %d\n", bodicky[it+1].y(), bodicky[it + 2].y());
			zet.push_back(surZ[it + 2]);
			zet.push_back(surZ[it + 1]);
		}

		stredne.push_back(bodicky[it + 1]); //ulozene stredne body z kazdeho trojuholnika
		printf("bodicky[it] %d bodicky[it+1] %d bodicky[it+2] %d\n", bodicky[it].y(), bodicky[it + 1].y(),bodicky[it+2].y());
		printf("strede %d \n", stredne[i].y());

		//hladame bod D (linearna interpolacia)
		QPoint pomD;
		pomD.setY(stredne[i].y());
		pomD.setX(bodicky[it].x() + ((bodicky[it + 1].y() - bodicky[it].y())*
			((bodicky[it + 2].x() - bodicky[it].x()) / (bodicky[it + 2].y() - bodicky[it].y()))));
		D.push_back(pomD);
		printf("D %d ", pomD.x());

		//ulozenie do vektorov trojuholniky ABD,ACD
		ABD.push_back(bodicky[it + 1]);
		ABD.push_back(bodicky[it]);
		ABD.push_back(pomD);
		ABDz.push_back(zet[i + 1]); ABDz.push_back(zet[i]); ABDz.push_back((zet[i]+zet[i + 2])/2);
		ADC.push_back(bodicky[it + 1]);
		ADC.push_back(bodicky[it + 2]);
		ADC.push_back(pomD);
		ADCz.push_back(zet[i + 1]); ADCz.push_back(zet[i + 2]); ADCz.push_back((zet[i] + zet[i + 2]) / 2);
		
		printf("i %d\n", i);
	}

	//vyplnenie 1. trojuholnika
	TH(ADC);
	int ya = tabulkaHran[1][0];
	for (int i = 0; i < ADC.size(); i++)
	{
		Phongov_osv_model(moznost);
		//QColor C(I[i]);
		QColor C(255,0,0);
		painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(ADC[i + 1]);	//vykreslenie bodu C
		zBuffer(I, ADCz[i + 1]);		//zapisanie do z-buffera

		//vypocitanie priesecnika
		QVector<int>priesecniky;
		priesecniky.clear();
		for (int j = 0; j < tabulkaHran.size(); j = j + 2)
		{
			int xa = 0, xaa = 0;
			xa = tabulkaHran[j][3] * ya - tabulkaHran[j][3] * tabulkaHran[j][0] + tabulkaHran[j][1];
			xaa = tabulkaHran[j + 1][3] * ya - tabulkaHran[j + 1][3] * tabulkaHran[j + 1][0] + tabulkaHran[j + 1][1];
			priesecniky.push_back(xa);
			priesecniky.push_back(xaa);
			painter.drawPoint(xa,ya);
			zBuffer(I, (ADCz[i + 1] + ADCz[i]) / 2.0);
			painter.drawPoint(xaa,ya);
			zBuffer(I, (ADCz[i + 1] + ADCz[i+2]) / 2.0);
			painter.drawLine(xa, ya, xaa, ya);
			zBuffer(I, (xa + xaa) / 2.0);
		}
		ya = ya + tabulkaHran[i][3];
	}
	
	//vyplnenie 2. trojuholnika
	TH(ABD);
	int ya2 = tabulkaHran[0][0];
	for (int i = 0; i < ABD.size(); i++)
	{
		Phongov_osv_model(moznost);
		//QColor C(I[i]);
		QColor C(255, 0, 0);
		painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
		painter.drawPoint(ABD[i]);	//vykreslenie bodu A
		zBuffer(I, ABDz[i]);		//zapisanie do z-buffera
		painter.drawPoint(ABD[i + 2]);	//vykreslenie bodu D
		zBuffer(I, ABDz[i + 2]);		//zapisanie do z-buffera
		painter.drawLine(ABD[i], ABD[i + 2]);

		//vypocitanie priesecnika
		QVector<int>priesecniky;
		priesecniky.clear();
		for (int j = 0; j < tabulkaHran.size(); j = j + 2)
		{
			int xa = 0, xaa = 0;
			xa = tabulkaHran[j][3] * ya2 - tabulkaHran[j][3] * tabulkaHran[j][0] + tabulkaHran[j][1];
			xaa = tabulkaHran[j + 1][3] * ya2 - tabulkaHran[j + 1][3] * tabulkaHran[j + 1][0] + tabulkaHran[j + 1][1];
			priesecniky.push_back(xa);
			priesecniky.push_back(xaa);
			painter.drawPoint(xa, ya);
			zBuffer(I, (ABDz[i + 1] + ABDz[i]) / 2.0);
			painter.drawPoint(xaa, ya2);
			zBuffer(I, (ABDz[i + 1] + ABDz[i + 2]) / 2.0);
			painter.drawLine(xa, ya2, xaa, ya2);
			zBuffer(I, (xa + xaa) / 2.0);
		}
		ya2 = ya2 + tabulkaHran[i][3];
		painter.drawPoint(ABD[i + 1]);	//vykreslenie bodu B
		zBuffer(I, ABDz[i + 1]);		//zapisanie do z-buffera
	}
}

void PaintWidget::DDA(QVector<QPoint>b)
{
	QPainter painter(&image);
	QColor C(255, 0, 0);
	painter.setPen(QPen(C, myPenWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
	painter.setBrush(QBrush(C));
	QMessageBox mbox;

	double dx, dy, steps, xx, yy, x, y;
	int x1, x2, y1, y2, x0, y0;
	for (int i = 0; i < b.size(); i++)
	{
		if (i != b.size() - 1)
		{
			x1 = b[i].x();
			y1 = b[i].y();
			x2 = b[i + 1].x();
			y2 = b[i + 1].y();
		}

		else
		{
			x1 = b[b.size() - 1].x();
			y1 = b[b.size() - 1].y();
			x2 = b[0].x();
			y2 = b[0].y();
		}

		dx = x2 - x1;
		dy = y2 - y1;

		if (fabs(dx) > fabs(dy))steps = fabs(dx);
		else steps = fabs(dy);

		xx = dx / steps;
		yy = dy / steps;

		x = x1;
		y = y1;

		for (int i = 1; i <= steps; i++)
		{
			painter.drawEllipse((int)x, (int)y, 2, 2);
			x += xx;
			y += yy;
		}
		painter.drawEllipse(x, y, 2, 2);
	}
	//	b.clear();
	update();
}

void PaintWidget::Konstantne()
{

}

void PaintWidget::otocenie_vlavo(int uhol)
{
	body_pom.clear();
	int Sx, Sy, X, Y;
	Sx = body[0].x();
	Sy = body[0].y();

	QPainter painter(&image);
	body_pom.push_back(body[0]);
	for (int i = 1; i < body.size(); i++)
	{
		X = ((int)((body[i].x() - Sx)*cos(M_PI / double(uhol)) + (body[i].y() - Sy)*sin(M_PI / double(uhol)) + Sx));
		Y = ((int)(-(body[i].x() - Sx)*sin(M_PI / double(uhol)) + (body[i].y() - Sy)*cos(M_PI / double(uhol)) + Sy));
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();
}

void PaintWidget::skalovanie(float koef)
{
	QPainter painter(&image);
	body_pom.clear();
	int Sx, Sy, X, Y, x0, y0;
	Sx = body[0].x();
	Sy = body[0].y();
	body_pom.push_back(body[0]);

	for (int i = 1; i < body.size(); i++)
	{
		x0 = body[i].x() - Sx;
		y0 = body[i].y() - Sy;

		x0 = (int)(koef*x0);
		y0 = (int)(koef*y0);

		X = x0 + Sx;
		Y = y0 + Sy;
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();
}

void PaintWidget::otocenie_vpravo(int uhol)
{
	body_pom.clear();
	int Sx, Sy, X, Y;
	Sx = body[0].x();
	Sy = body[0].y();

	QPainter painter(&image);
	body_pom.push_back(body[0]);
	for (int i = 1; i < body.size(); i++)
	{
		X = ((int)((body[i].x() - Sx)*cos(M_PI / uhol) - (body[i].y() - Sy)*sin(M_PI / uhol) + Sx));
		Y = ((int)((body[i].x() - Sx)*sin(M_PI / uhol) + (body[i].y() - Sy)*cos(M_PI / uhol) + Sy));
		body_pom.push_back(QPoint(X, Y));
	}
	body.clear();
	body = body_pom;

	update();
}

void PaintWidget::posunutie()
{
	clearImage();
	body_pom.clear();
	Bodiky.clear();

	QPoint Z, K;
	Z = posunutie_body[0];
	K = posunutie_body[posunutie_body.size() - 1];

	int posunX = K.x() - Z.x();
	int posunY = K.y() - Z.y();
	for (int i = 0; i < body.size(); i++)
	{
		int noveX, noveY;
		noveX = body[i].x() + posunX;
		noveY = body[i].y() + posunY;
		body_pom.push_back(QPoint(noveX, noveY));
	}
	
	for (int i = 0; i < indexy.size(); i++)
	{
		int it =indexy[i];
		Bodiky.push_back(body_pom[it]);
	}
	DDA(Bodiky);
	
	body.clear();
	body = body_pom;
	posunutie_body.clear();
}